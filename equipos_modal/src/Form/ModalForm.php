<?php

namespace Drupal\equipos_modal\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;

class ModalForm extends BaseForm {
  public function getFormId() {
    return 'equipos_modal_modalform';
  }
  
  public function ajaxModalSubmit(array &$form, FormStateInterface $form_state) {
    $status = [
      '#markup' => $this->t('Submitted'),
      '#prefix' => '<div id="modal-status">',
      '#suffix' => '</div>',
    ];
    
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#modal-status', $status));
    
    return $response;
  }
  
  public function ajaxModalClose(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();    
    
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }
  
  public function ajaxModalSend(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    
    $response->addCommand(new CloseModalDialogCommand());
    
    $element = [
      '#markup' => $this->t('Message from modal form.'),
      '#prefix' => '<div id="baseform-status">',
      '#suffix' => '</div>',
    ];
    $response->addCommand(new ReplaceCommand('#baseform-status', $element));
    
    return $response;
  }
  
  public function ajaxBaseSend(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    
    $response->addCommand(new CloseModalDialogCommand());
    $response->addCommand(new InvokeCommand('#baseform-sendbutton', 'click'));
    
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['modaltext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Intro some text to submit'),
      '#default_value' => $this->t('Test Text'),
    ];
    
    $form['status'] = [
      '#markup' => $this->t('No Submitted'),
      '#prefix' => '<div id="modal-status">',
      '#suffix' => '</div>',
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    
    $form['actions']['close'] = [
      '#type' => 'button',
      '#value' => $this->t('CloseModal'),
      '#ajax' => [
        'callback' => [$this, 'ajaxModalClose'],
      ],
    ];
    
    $form['actions']['modalsend'] = [
      '#type' => 'button',
      '#value' => $this->t('Modal Send'),
      '#ajax' => [
        'callback' => [$this, 'ajaxModalSend'],
      ],
    ];
    
    $form['actions']['basesend'] = [
      '#type' => 'button',
      '#value' => $this->t('Base Send'),
      '#ajax' => [
        'callback' => [$this, 'ajaxBaseSend'],
      ],
    ];
    
    $form['actions']['ajaxSubmit'] = [
      '#type' => 'submit',
      '#value' => $this->t('AjaxSubmit'),
      '#ajax' => [
        'callback' => [$this, 'ajaxModalSubmit'],
      ],
    ];
    
    return $form;
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {
    drupal_set_message('ModalText: ' . $form_state->getValue('modaltext'));
    drupal_set_message($this->t('Modal form submitted.'));
    $form_state->setRedirect('equipos_modal.baseform');
  }
}