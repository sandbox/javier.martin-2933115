<?php

namespace Drupal\equipos_modal\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;

class BaseForm extends FormBase {
  protected $form_builder;
  
  public function __construct(FormBuilderInterface $form_builder) {
    $this->form_builder = $form_builder;
  }
  
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('form_builder')
    );
  }
  
  public function getFormId() {
    return 'equipos_modal_baseform';
  }
  
  public function ajaxButton(array &$form, FormStateInterface $form_state) {
    $form = $this->form_builder->getForm('Drupal\equipos_modal\Form\ModalForm');
    
    $response = new AjaxResponse();
    $response->addCommand(new OpenModalDialogCommand('Modal Test Form', $form));
    
    return $response;
  }
  
  public function ajaxSend(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    
    $response->addCommand(new ReplaceCommand('#baseform-status', $form['status']));
    return $response;
  }
  
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $message = $this->t('Empty Message');
    
    $element = $form_state->getTriggeringElement();   
    if (!empty($element)) {
      $message = $this->t('Message from Base Form catching triggering element.');
    }
    
    $form['status'] = [
      '#markup' => $message,
      '#prefix' => '<div id="baseform-status">',
      '#suffix' => '</div>',
    ];
    
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['actions']['button'] = [
      '#type' => 'button',
      '#value' => $this->t('Open Modal - Button'),
      '#ajax' => [
        'callback' => [$this, 'ajaxButton'],
      ],
    ];
    
    $form['actions']['link'] = [
      '#type' => 'link',
      '#title' => $this->t('Open Modal - Link'),
      '#url' => Url::fromRoute('equipos_modal.modalform'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button',
        ],
        'data-dialog-type' => 'modal',
      ],
    ];
    
    $form['actions']['send'] = [
      '#type' => 'button',
      '#value' => $this->t('Send Message'),
      '#ajax' => [
        'callback' => [$this, 'ajaxSend'],
      ],
      '#attributes' => [
        'id' => 'baseform-sendbutton',
      ],
    ];
    
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) { }
  
}